import { Request, Response, response } from 'express';
import { read } from 'fs';

var patientsDB = [
    {
        "index" : 0,
        "name": "Annie Hall",
        "picture": "http://placehold.it/32x32",
        "about" : "Cita Finalizada",
        "isToday": false,
        "isOnline": false
    },
    {
        "index" : 1,
        "name": "Jodee Furrow",
        "picture": "http://placehold.it/32x32",
        "about" : "Hoy 2:00pm",
        "isToday": true,
        "isOnline": true
    },
    {
        "index" : 2,
        "name": "Karl Nyland",
        "picture": "http://placehold.it/32x32",
        "about" : "Cita: Enero 20, 8:00 am",
        "isToday": false,
        "isOnline": true
    },
    {
        "index" : 3,
        "name": "Billie Janzen",
        "picture": "http://placehold.it/32x32",
        "about" : "Cita: Enero 22, 08:00 am",
        "isToday": false,
        "isOnline": true
    },
    {
        "index": 4,
        "name": "Jack Ahlers",
        "picture": "http://placehold.it/32x32",
        "about" : "Cita: Enero 22, 10:00 am",
        "isToday": false,
        "isOnline": true
    },
]

export function getPatients(req: Request, res: Response): Response {
    res.contentType('application/json');
    return res.send(JSON.stringify(patientsDB));
}

export function getPatient(req: Request, res: Response): Response {
    let index: number = Number(req.params.id);
    console.log("you sent",index)
    if(typeof patientsDB[index] === 'undefined'){
        return res.send("Index Undefinied");
    }
    else {
        return res.send(patientsDB[index]);
    }
}

export function updatePatient(req: Request, res: Response) : Response{
    var json = req.body;
    if( req.body.index <= patientsDB.length - 1 && req.body.index >= 0){
        console.log(patientsDB[req.body.index].name)
        patientsDB[req.body.index].name = req.body.name
        console.log(patientsDB[req.body.index].name)
        patientsDB[req.body.index].about = req.body.about    
        return res.send("Sucessfull changes!")
    }
    else{
        return res.send("Not Sucessfull changes!")
    }
}