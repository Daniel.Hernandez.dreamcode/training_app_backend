import app from '../../../../../app';
import { Router } from 'express';
import { getPatients,getPatient, updatePatient } from '../controllers/patients.controller';

const router: Router = Router();
export default router;

router.route("/patients").get(getPatients);

router.route("/patients/:id").post(getPatient);

router.route("/patientUpdate").put(updatePatient);