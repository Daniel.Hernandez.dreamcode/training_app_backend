import app from './app'

async function main(){
    app.listen(app.get("port"), () => {
    console.log("Server running at port", app.get("port"));
    })}

main();