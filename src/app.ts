import express from 'express';
import cors from 'cors';
import { Application } from 'express-serve-static-core';
import routes from './com/dreamcode/medikos/patients/routes/routes';

const app: Application = express(); 
export default app;

//Settings
app.set("port","3600")

//Midleware
app.use(cors());
app.use(express.json())


//Router
app.use("/api", routes);
